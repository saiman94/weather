//
//  NowWeather.swift
//  Weather v.3
//
//  Created by Артур on 31.03.2021.
//

import Foundation
import UIKit

struct NowWeather: Codable {
    var list: [WeatherNow]
    var city: NowWeatherCity
    
    var listSortedByDays: [[WeatherNow]] {
        var listSortedByDays = [[WeatherNow]]()
        
        for weather in list {
            
            
            let timeZone = TimeZone(secondsFromGMT: city.timezone)!
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = timeZone
            dateFormatter.dateFormat = "dd"
                        
            if listSortedByDays.count > 0 {
                if dateFormatter.string(from: weather.dt) == dateFormatter.string(from: listSortedByDays.last?.last?.dt ?? Date()) {
                    listSortedByDays[listSortedByDays.count - 1].append(weather)
                } else {
                    listSortedByDays.append([weather])
                }
            } else {
                listSortedByDays.append([weather])
            }
        }
        return listSortedByDays
    }
}

struct NowWeatherCity: Codable {
    var name: String?
    var coord: Coord
    var sunrise: Date
    var sunset: Date
    var timezone: Int
    var country: String
    
    var sunriseString: String {
        let delta = TimeInterval(TimeZone(secondsFromGMT: timezone)!.secondsFromGMT(for: sunrise) - 0)
        let correctDate = sunrise.addingTimeInterval(delta)
        let dateDormatter = DateFormatter()
        dateDormatter.dateFormat = "HH:mm"
        print("time is: \(dateDormatter.string(from: correctDate))")
        return dateDormatter.string(from: correctDate)
    }
}
struct WeatherNow: Codable {
    
    var main: Main
    var weather: [Weather]
    var dt: Date
}

struct Weather: Codable {
    var main: String
   
    
    var weatherText: String? {
        switch main {
        case "Clouds":
            return ("Cloudy")
        case "Clear":
            return ("Sunny")
        case "Rain":
            return ("Raining")
        case "Snow":
            return ("Snowing")
        default:
            return nil
        }
    }
    var weatherImage: UIImage? {
            switch main {
            case "Clouds":
                return UIImage(named: "cloudy")
            case "Clear":
                return UIImage(named: "sunny")
            case "Rain":
                return UIImage(named: "raining")
//            case "":
//                return UIImage(named: "misty")
//            case "":
//                return UIImage(named: "windy")
            case "Snow":
                return UIImage(named: "snowing")
            default:
                return nil
        }
    }
}
