//
//  CityWeather.swift
//  Weather v.3
//
//  Created by Артур on 15.03.2021.
//

import Foundation

struct CityWeatherList: Codable {
    var list: [CityWeather]
}
struct CityWeather: Codable {
    var id: Int
    var main: Main
    var name: String
    var wind: Wind
    var sys: Sys
    var coord: Coord
}
struct Coord: Codable {
    var lat: Double
    var lon: Double
}
struct Sys: Codable {
    var sunrise: Date
    var sunset: Date
    var timezone: Int
    
    var sunriseString: String {
        let delta = TimeInterval(TimeZone(secondsFromGMT: timezone)!.secondsFromGMT(for: sunrise) - 0)
        let correctDate = sunrise.addingTimeInterval(delta)
        let dateDormatter = DateFormatter()
        dateDormatter.dateFormat = "HH:mm"
        print("time is: \(dateDormatter.string(from: correctDate))")
        return dateDormatter.string(from: correctDate)
    }
}
struct Main: Codable {
    var temp: Float
    var cels: String {
        let tempDouble = Double(temp).rounded()
        return String(Int(tempDouble) - 273) + "°C"
    }
}

struct Wind: Codable {
    var deg: Double
    var speed: Float
    var windSpeed: String {
        
        let windDirection: String
        switch deg {
        case 0..<22.5, 337.5...360:
            windDirection = "n"
            // cевер
        case 22.5..<67.5:
            // северо-восток
            windDirection = "ne"
        case 67.5..<112.5:
            // восток
            windDirection = "e"
        case 112.5..<157.5:
            //юго-восток
            windDirection = "se"
        case 157.5..<202.5:
            //юг
            windDirection = "s"
        case 202.5..<247.5:
            //юго-запад
            windDirection = "sw"
        case 247.5..<292.5:
            //запад
            windDirection = "w"
        case 292.5..<337.5:
            //северо-запад
            windDirection = "nw"
        default:
            windDirection = "No data"
        }
        
        return windDirection + " " + String(Int(speed)) + " m/s"
    }
}
