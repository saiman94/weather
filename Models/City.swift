//
//  City.swift
//  Weather v.3
//
//  Created by Артур on 14.03.2021.
//

import Foundation
import RealmSwift

struct CityList {
    var list: [City]
    
    init(from: RealmCityList) {
        self.list = from.list.map { City(from: $0) }
    }
    
    init(from: RealmCityCheckedList) {
        self.list = from.list.map { City(from: $0) }
    }
    
    init(from: [City]) {
        self.list = from
    }
}

struct City: Codable, Equatable {
    static func == (lhs: City, rhs: City) -> Bool {
        return lhs.city.id.number == rhs.city.id.number
    }
    
        var city: CityData
    
    init(from: RealmCity) {
        
        city = CityData(name: from.name, number: from.id, country: from.country)
        
    }
}
struct CityData: Codable {
    var name: String
    var country: String
    var id: JsonCityId
    
    var countryFlag: String? {
        var flag = ""
        let base : UInt32 = 127397
        for scalar in country.unicodeScalars {
            if let unicodeScalar = UnicodeScalar(base + scalar.value) { flag.unicodeScalars.append(unicodeScalar) }
        }
        return String(flag)
    }
    
    init(name: String, number: String, country: String) {
        self.name = name
        self.id = JsonCityId(number: number)
        self.country = country
    }
}
struct JsonCityId: Codable {
    
    var number: String
    
    var id: Int {
        return Int(number) ?? 0
    }
    
    enum CodingKeys: String, CodingKey {
        case number = "$numberLong"
    }
    init(number: String) {
        self.number = number
    }
}

// MARK: - Realm models
class RealmCityList: Object {
    dynamic var list = List<RealmCity>()
    @objc dynamic var key = "RealmCityList"
    
    override static func primaryKey() -> String? {
        return "key"
    }
    
    convenience init(from: CityList) {
        self.init()
        list.append(objectsIn: from.list.map { RealmCity(from: $0) })
    }
    
    convenience init(from: [RealmCity]) {
        self.init()
        list.append(objectsIn: from)
    }
}

class RealmCityCheckedList: Object {
    dynamic var list = List<RealmCity>()
    @objc dynamic var key = "RealmCityCheckedList"
    
    override static func primaryKey() -> String? {
        return "key"
    }
    
    convenience init(from: CityList) {
        self.init()
        list.append(objectsIn: from.list.map { RealmCity(from: $0) })
    }
}

class RealmCity: Object {
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var country: String = ""
    
    convenience init(from: City) {
        self.init()
        self.id = from.city.id.number
        self.name = from.city.name
        self.country = from.city.country
    }
    
    convenience init(from: WeatherByCoordinate) {
        self.init()
        self.id = String(from.id)
        self.name = from.name
    }
}
