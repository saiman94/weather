//
//  RealmManager.swift
//  Weather v.3
//
//  Created by Артур on 25.03.2021.
//

import Foundation
import RealmSwift

class RealmManager {
    
    func write<T: Object>(object: [T]) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(object, update: .modified)
        }
    }
    
    func appendCity(realmCity: RealmCity) {
        let realm = try! Realm()
        if let realmCityList = obtain(objectType: RealmCityCheckedList.self)?.first {
            try! realm.write {
                if !realmCityList.list.contains(where: { $0.id == realmCity.id }) {
                    realmCityList.list.append(realmCity)
                    realm.add(realmCityList, update: .modified)
                }
            }
        } else {
            let realmCityList = RealmCityCheckedList()
            try! realm.write {
                realmCityList.list.append(realmCity)
                realm.add(realmCityList, update: .modified)
            }
        }
    }
    
    func obtain<T: Object>(objectType: T.Type) -> Results<T>? {
        let realm = try! Realm()
        return realm.objects(objectType)
    }
    
    func delete<T: Object>(objects: [T]) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(objects)
        }
    }
}
