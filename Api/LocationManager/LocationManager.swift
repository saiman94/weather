//
//  LocationManager.swift
//  Weather v.3
//
//  Created by Артур on 14.04.2021.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {
    private let manager = CLLocationManager()
    private var locationNeedsUpdate = true
    var gotLocation: ((CLLocation) -> ())?
    
    
    func startLocating() {
        print("status: \(CLLocationManager.authorizationStatus())")
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            print("notDetermined")
        case .denied:
            print("denied")
        case .restricted:
            print("restricted")
        default:
            break
        }
        
        if CLLocationManager.locationServicesEnabled() {
            manager.delegate = self
            manager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        }
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locationNeedsUpdate { return }
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        manager.stopUpdatingLocation()
        self.locationNeedsUpdate = false
        
        let coordinates = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        gotLocation?(coordinates)
//        print(coordinates)
    }
}
