//
//  ApiCore.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import Foundation

enum WeatherResult<T: Codable> {
    case success(object: T)
    case error(errorName: String)
}

class ApiCore {
    // дженерик
    // замыкания (в том числе escaping (сбегающее))
    func sendRequest<T: Codable>(url: URL, completion: @escaping (WeatherResult<T>) -> Void) {
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let statusCode = (response as? HTTPURLResponse)?.statusCode
            print("status code: \(statusCode)")
            
            
            switch statusCode {
            case 401:
                DispatchQueue.main.async {
                    completion(WeatherResult.error(errorName: "Ошибка 401"))
                }
            case 404:
                DispatchQueue.main.async {
                    completion(WeatherResult.error(errorName: "Ошибка 404"))
                }
            case 500:
                DispatchQueue.main.async {
                    completion(WeatherResult.error(errorName: "Ошибка 500"))
                }
            default:
                do {
                    let json: [String : Any] = try JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as! [String : Any]
                    print("json: \(json)")
                    
                    let citiesList = try JSONDecoder().decode(T.self, from: data)
                    DispatchQueue.main.async {
                        completion(WeatherResult.success(object: citiesList))
                    }
                } catch (let error) {
                    print ("parsing \(error)")
                    DispatchQueue.main.async {
                        completion(WeatherResult.error(errorName: "Что то пошло не так"))
                    }
                }
            }
        }.resume()
    }
}
