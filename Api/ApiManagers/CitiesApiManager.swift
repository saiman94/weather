//
//  CitiesApiManager.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import Foundation
protocol CitiesApiManager {
    func getCities(citiesIdsString: String, completion: @escaping (WeatherResult<CityWeatherList>) -> Void)
    func getWeekForecast(id: Int, completion: @escaping(WeatherResult<NowWeather>) -> Void)
    func coordinateWeather(lon: Double, lat: Double, completion: @escaping(WeatherResult<WeatherByCoordinate>) -> Void )
}

