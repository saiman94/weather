//
//  ApiRequests.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import Foundation
class ApiRequests {
    let apiCore = ApiCore()
}

extension ApiRequests: CitiesApiManager {
    
    
    func getCities(citiesIdsString: String, completion: @escaping (WeatherResult<CityWeatherList>) -> Void) {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/group?appid=d37bcf8d93b6901ff04244e5a8413509&id=\(citiesIdsString)")!
        apiCore.sendRequest(url: url, completion: completion)
    }
    
    func getWeekForecast(id: Int, completion: @escaping(WeatherResult<NowWeather>) -> Void) {
        let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?appid=d37bcf8d93b6901ff04244e5a8413509&units=metric&id=\(id)")!
        apiCore.sendRequest(url: url, completion: completion)
    }
    func coordinateWeather(lon: Double, lat: Double, completion: @escaping(WeatherResult<WeatherByCoordinate>) -> Void ) {
        
        let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&units=metric&appid=d37bcf8d93b6901ff04244e5a8413509")!
        apiCore.sendRequest(url: url, completion: completion)
        
    }
    
}
