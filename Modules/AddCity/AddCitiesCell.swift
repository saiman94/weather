//
//  AddCitiesCell.swift
//  Weather v.3
//
//  Created by Артур on 14.03.2021.
//

import UIKit

class AddCitiesCell: UITableViewCell {

    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet var cityNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tintColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
        cityNameLabel.textColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
        countryNameLabel.textColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
    }
    
    func fillCell(city: City) {
        cityNameLabel.text = city.city.name
        countryNameLabel.text = city.city.countryFlag! + " " + "(\(city.city.country))"
    }
}
