//
//  AddCitiesPresented.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import UIKit

class AddCitiesPresenter {
    
    var cities = [City]()
    var citiesChecked = [City]()
    var citiesSearch = [City]()
    
    var delegate: AddCitiesViewControllerDelegate?
    weak var addCitiesViewController: AddCitiesViewController?
    
    let localJsonManager: LocalJsonManager
    let realmManager: RealmManager
    
    init(viewController: AddCitiesViewController, delegate: AddCitiesViewControllerDelegate?, localJsonManager: LocalJsonManager = LocalJsonManager(), realmManager: RealmManager = RealmManager()) {
        self.addCitiesViewController = viewController
        self.delegate = delegate
        self.localJsonManager = localJsonManager
        self.realmManager = realmManager
    }
    
    func viewIsReady() {
        getCities()
    }
    
    func getCities() {
        DispatchQueue.global(qos: .userInitiated).async {
            if let realmCitiesList = self.realmManager.obtain(objectType: RealmCityList.self)?.first {
                self.cities = CityList(from: realmCitiesList).list
            }
            
            if let realmCitiesCheckedList = self.realmManager.obtain(objectType: RealmCityCheckedList.self)?.first {
                self.citiesChecked = CityList(from: realmCitiesCheckedList).list
            }
            
            DispatchQueue.main.async {
                self.citiesSearch = self.cities
                self.createSections()
            }
        }
    }
    
    func createSections() {
        var section = AddCitiesSection (items: [])
        let items = citiesSearch.map { city -> AddCitiesItem in
            let isChecked = citiesChecked.contains(city)
            return AddCitiesItem.city(city: city, isChecked: isChecked)
        }
        section.items = items
        addCitiesViewController?.updateView(sections: [section])
    }
    
    func saveButtonDidTapped() {
        let realmCitiesChecked = RealmCityCheckedList(from: CityList(from: citiesChecked))
        realmManager.write(object: [realmCitiesChecked])
        delegate?.updateViewState()
        addCitiesViewController?.navigationController?.popViewController(animated: true)
    }
    
    func cellDidTapped(indexPath: IndexPath) {
        if !citiesChecked.contains(citiesSearch[indexPath.row]) {
            citiesChecked.append(citiesSearch[indexPath.row])
        } else {
            if let index = citiesChecked.firstIndex(of: citiesSearch[indexPath.row]) {
                citiesChecked.remove(at: index)
            }
        }
        createSections()
    }
    func didSearch(text: String) {
        if text.isEmpty {
            citiesSearch = cities
        } else {
            citiesSearch = []
            for city in cities {
                if city.city.name.lowercased().contains(text.lowercased()) {
                    citiesSearch.append(city)
                }
            }
        }
        createSections()
    }
}
