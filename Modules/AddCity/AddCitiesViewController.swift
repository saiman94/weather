//
//  AddCitiesViewController.swift
//  Weather v.3
//
//  Created by Артур on 14.03.2021.
//

import UIKit

protocol AddCitiesViewControllerDelegate {
    func updateViewState()
}

class AddCitiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    @IBOutlet var tableView: UITableView!
    var presenter: AddCitiesPresenter?
    
    init(delegate: AddCitiesViewControllerDelegate) {
        super.init(nibName: "AddCitiesViewController", bundle: nil)
        presenter = AddCitiesPresenter(viewController: self, delegate: delegate)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var sections = [AddCitiesSection]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Choose a city"
        
        let topColor = UIColor(red: 255/255, green: 241/255, blue: 235/255, alpha: 1).cgColor
        let bottomColor = UIColor(red: 200/255, green: 230/255, blue: 244/255, alpha: 1).cgColor
        
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor, bottomColor]
        gradientLayer.locations = [0, 0.95, 1]
        view.layer.insertSublayer(gradientLayer, at: 0)
                
        let saveButton = UIBarButtonItem.init(barButtonSystemItem: .save, target: self, action: #selector(saveButtonDidTapped))
        navigationItem.rightBarButtonItem = saveButton

        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Type something here to search"
        navigationItem.searchController = search
        
        tableView.register(UINib(nibName: "AddCitiesCell", bundle: nil), forCellReuseIdentifier: "AddCitiesCell")
        
        presenter?.viewIsReady()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        let city = sections[indexPath.section].items[indexPath.row]
        switch city {
        case .city(let city, let isChecked):
            cell = tableView.dequeueReusableCell(withIdentifier: "AddCitiesCell") as? AddCitiesCell
            (cell as? AddCitiesCell)?.fillCell(city: city)
            if isChecked {
                cell?.accessoryType = .checkmark
            } else {
                cell?.accessoryType = .none
            }
        }
        return cell ?? UITableViewCell()
        
    }
    
    func updateView(sections: [AddCitiesSection]) {
        self.sections = sections
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.cellDidTapped(indexPath: indexPath)
    }
    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text ?? ""
        
        presenter?.didSearch(text: text)
        
    }
    @objc func saveButtonDidTapped() {
        presenter?.saveButtonDidTapped()
    }
}

