//
//  AddCitiesModel.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import Foundation
struct AddCitiesSection {
    var items: [AddCitiesItem]
}
enum AddCitiesItem {
    case city(city: City, isChecked: Bool)
}
