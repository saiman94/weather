//
//  CitiesWeatherViewController.swift
//  Weather v.3
//
//  Created by Артур on 14.03.2021.
//

import UIKit

class CityWeatherViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    var sections = [CityWeatherSection]()
    
    lazy var presenter = CityWeatherPresenter(viewController: self)
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Weather"
        
        let topColor = UIColor(red: 255/255, green: 241/255, blue: 235/255, alpha: 1).cgColor
        let bottomColor = UIColor(red: 200/255, green: 230/255, blue: 244/255, alpha: 1).cgColor
        let navigationButtonsAndTitleColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor, bottomColor]
        gradientLayer.locations = [0, 0.95, 1]

        view.layer.insertSublayer(gradientLayer, at: 0)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: navigationButtonsAndTitleColor]
        navigationController?.navigationBar.tintColor = navigationButtonsAndTitleColor
        
        
        tableView.register(UINib(nibName: "CityWeatherCell", bundle: nil), forCellReuseIdentifier: "CityWeatherCell")
        let addButton = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(addButtonDidTapped))
        addButton.style = .done
        navigationItem.rightBarButtonItem = addButton
        
        presenter.viewIsReady()
    }
    
    @objc func addButtonDidTapped() {
        presenter.addButtonDidTapped()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: UITableViewCell?
        let city = sections[indexPath.section].items[indexPath.row]
        
        switch city {
        case .cityWeather(let cityWeather):
            cell = tableView.dequeueReusableCell(withIdentifier: "CityWeatherCell") as? CityWeatherCell
            (cell as? CityWeatherCell)?.fillCell(list: cityWeather)
        }
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if case CityWeatherItem.cityWeather(let cityWeather) = sections[indexPath.section].items[indexPath.row] {
            presenter.deleteCityFromRealm(cityWeather: cityWeather)
            
            sections[indexPath.section].items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = sections[indexPath.section].items[indexPath.row]
        guard case CityWeatherItem.cityWeather(let cityWeather) = item else {
            return
        }
        let id = cityWeather.id
        
        let cityDetailedViewController = CityDetailedViewController(id: id)
        navigationController?.pushViewController(cityDetailedViewController, animated: true)
    }
    
    func upDateView(sections: [CityWeatherSection]) {
        self.sections = sections
        tableView.reloadData()
        
    }
    func showError(errorName: String) {
        let alertController = UIAlertController(title: "Error!", message: errorName, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
