//
//  CityWeatherCell.swift
//  Weather v.3
//
//  Created by Артур on 14.03.2021.
//

import UIKit

class CityWeatherCell: UITableViewCell {

    @IBOutlet var name: UILabel!
    @IBOutlet var temp: UILabel!
    @IBOutlet var wind: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        name.textColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
        temp.textColor = UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1)
        wind.textColor = UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1)
        
    }
    
    func fillCell(list: CityWeather) {
        name.text = list.name
        temp.text = list.main.cels
        wind.text = list.wind.windSpeed
    }
}
