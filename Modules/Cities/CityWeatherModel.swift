//
//  CityWeatherModel.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import Foundation

struct CityWeatherSection {
    var items: [CityWeatherItem]
}

enum CityWeatherItem {
    case cityWeather(cityWeather: CityWeather)
}
