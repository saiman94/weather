//
//  CityWeatherPresenter.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import UIKit

class CityWeatherPresenter: AddCitiesViewControllerDelegate {
        
    weak var cityWeatherViewController: CityWeatherViewController?
    
    let apiManager: CitiesApiManager
    let realmManager: RealmManager
    var realmCitiesList: RealmCityCheckedList?
    var locationManager: LocationManager
    
    init(viewController: CityWeatherViewController, apiManager: CitiesApiManager = ApiRequests(), realmManager: RealmManager = RealmManager(), locationManager: LocationManager = LocationManager()) {
        self.cityWeatherViewController = viewController
        self.apiManager = apiManager
        self.realmManager = realmManager
        self.locationManager = locationManager
    }
    
    func viewIsReady() {
//        updateViewState()
        
        locationManager.gotLocation = { location in
            self.apiManager.coordinateWeather(lon: location.coordinate.longitude, lat: location.coordinate.latitude, completion: { [weak self] result in
                switch result {
                case .success(let object):
                    
                    let realmCity = RealmCity(from: object)
                    self?.realmManager.appendCity(realmCity: realmCity)
                    self?.updateViewState()

                case .error(let errorName):
                    break
                }
            })
        }
        locationManager.startLocating()
    }
    
    func addButtonDidTapped() {
        let addCitiesViewController = AddCitiesViewController(delegate: self)
        cityWeatherViewController?.navigationController?.pushViewController(addCitiesViewController, animated: true)
    }
    
    func createSections(citiesWeather: [CityWeather]) {
        var citiesSection = CityWeatherSection(items: [])
        citiesSection.items = citiesWeather.map { CityWeatherItem.cityWeather(cityWeather: $0) }
        self.cityWeatherViewController?.upDateView(sections: [citiesSection])
    }
    
    func updateViewState() {
        realmCitiesList = realmManager.obtain(objectType: RealmCityCheckedList.self)?.first
        
        if let realmCitiesList = realmCitiesList, !realmCitiesList.list.isEmpty {
            let citiesList = CityList(from: realmCitiesList)
            
            let citiesIdsStringArray = citiesList.list.map {$0.city.id.number}
            let citiesIdsString = citiesIdsStringArray.reduce("", { $0 == "" ? $1 : $0 + "," + $1 } )
            
            apiManager.getCities(citiesIdsString: citiesIdsString, completion: { [weak self] result in
                
                switch result {
                case .success(let object):
                    self?.createSections(citiesWeather: object.list)
                case .error(let errorName):
                    self?.cityWeatherViewController?.showError(errorName: errorName)
                // здесь ошибка будет
                }
            })
        }
    }
    func deleteCityFromRealm(cityWeather: CityWeather) {
        if let objectsToDelete = realmCitiesList?.list.first(where: { Int($0.id) == cityWeather.id }) {
            realmManager.delete(objects: [objectsToDelete])
        }
    }
}
