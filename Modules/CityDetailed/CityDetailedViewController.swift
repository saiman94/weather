//
//  CityDetailedViewController.swift
//  Weather v.3
//
//  Created by Артур on 30.03.2021.
//

import UIKit

class CityDetailedViewController: UIViewController, UICollectionViewDataSource {

    var presenter: CityDetailedPresenter?
    var items = [WeatherNowItem]()
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var weatherImageView: UIImageView!
    @IBOutlet var weatherNameLabel: UILabel!
    @IBOutlet weak var weatherTimeLabel: UILabel!
    @IBOutlet weak var collectionViewGradientView: UIView!
    @IBOutlet var dateFieldLabel: UILabel!
    @IBOutlet var leftDateButton: UIButton!
    @IBOutlet var rightDateButton: UIButton!
    
    @IBAction func leftDateButtonDidTapped() {
        presenter?.previousDayButtonDidTapped()
    }
    @IBAction func rightDateButtonDidTapped() {
        presenter?.nextDayButtonDidTapped()
    }
    
    init(id: Int) {
        super.init(nibName: "CityDetailedViewController", bundle: nil)
        presenter = CityDetailedPresenter(viewController: self, id: id)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib(nibName: "WeatherTimeCell", bundle: nil), forCellWithReuseIdentifier: "WeatherTimeCell")
        dateFieldLabel.textColor = UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1)
        weatherNameLabel.textColor = UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1)
        let topColor = UIColor(red: 255/255, green: 241/255, blue: 235/255, alpha: 1).cgColor
        let bottomColor = UIColor(red: 200/255, green: 230/255, blue: 244/255, alpha: 1).cgColor
        let navigationButtonsAndTitleColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [topColor, bottomColor, bottomColor]
        gradientLayer.locations = [0, 0.95, 1]

        view.layer.insertSublayer(gradientLayer, at: 0)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: navigationButtonsAndTitleColor]
        navigationController?.navigationBar.tintColor = navigationButtonsAndTitleColor
        presenter?.viewIsReady()
        
        let locationBarButton = UIBarButtonItem(image: UIImage(named: "locationButton"), style: .plain, target: self, action: #selector (locationBarButtonDidTapped))
        navigationItem.rightBarButtonItem = locationBarButton
        
        leftDateButton.setImage(UIImage(named: "leftDateButton"), for: .normal)
        leftDateButton.setTitle(nil, for: .normal)
        leftDateButton.tintColor = UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1)
        rightDateButton.setImage(UIImage(named: "rightDateButton"), for: .normal)
        rightDateButton.setTitle(nil, for: .normal)
        rightDateButton.tintColor = UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1)
        
        weatherTimeLabel.alpha = 0
        dateFieldLabel.alpha = 0
        weatherNameLabel.alpha = 0
        collectionView.alpha = 0
        weatherImageView.alpha = 0
        leftDateButton.alpha = 0
        rightDateButton.alpha = 0
        collectionViewGradientView.alpha = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        let gradientView = UIView(frame: collectionViewGradientView.bounds)
        collectionViewGradientView.backgroundColor = .clear
        let collectionViewTopColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.05).cgColor
        let collectionViewBottomColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1).cgColor
        
        let collectionViewGradientLayer = CAGradientLayer()
        collectionViewGradientLayer.frame = collectionViewGradientView.bounds
        collectionViewGradientLayer.colors = [collectionViewTopColor, collectionViewBottomColor]
        collectionViewGradientLayer.locations = [0, 1]
        collectionViewGradientLayer.startPoint = CGPoint(x: 1, y: 0)
        collectionViewGradientView.layer.insertSublayer(collectionViewGradientLayer, at: 0)
        collectionViewGradientView.layer.borderWidth = 2
        collectionViewGradientView.layer.borderColor = UIColor(red: 207/255, green: 228/255, blue: 237/255, alpha: 1).cgColor
    }
    
    @objc func locationBarButtonDidTapped() {
        presenter?.locationBarButtonDidTapped()
    }
    
    func upDateItems(items:[WeatherNowItem]) {
        self.items = items
        collectionView.reloadData()
    }
    
    func updateDateLabelAndButtons(dateText: String,
                                   shouldHidePreviuosDayButton: Bool = false,
                                   shouldHideNextDayButton: Bool = false) {
        
//        dateFieldLabel.text = dateText
//        dateFieldLabel.font = UIFont(name: "Manrope-Light", size: 20)
        
        let attributes: NSDictionary = [
            NSAttributedString.Key.font: UIFont(name: "Manrope-Light", size: 20) as Any,
            NSAttributedString.Key.foregroundColor: UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1),
            NSAttributedString.Key.kern:CGFloat(2)
        ]
        let attributedTitle = NSAttributedString(string: dateText, attributes: attributes as! [NSAttributedString.Key : Any])
        dateFieldLabel.attributedText = attributedTitle
        
//        weatherTimeLabel.attributedText = attributedTitle
        
        leftDateButton.isHidden = shouldHidePreviuosDayButton
        rightDateButton.isHidden = shouldHideNextDayButton
    }
    
    func upDateView(time: String, image: UIImage?, text: String?, name: String?) {
        weatherTimeLabel.text = time
        weatherImageView.image = image
        weatherNameLabel.text = text
        self.navigationItem.title = name
        
        UIView.animate(withDuration: 0.3, animations: {
            self.weatherTimeLabel.alpha = 1
            self.dateFieldLabel.alpha = 1
            self.weatherNameLabel.alpha = 1
            self.collectionView.alpha = 1
            self.weatherImageView.alpha = 1
            self.leftDateButton.alpha = 1
            self.rightDateButton.alpha = 1
            self.collectionViewGradientView.alpha = 1
        })
        
        
        let attributes: NSDictionary = [
            NSAttributedString.Key.font: UIFont(name: "Manrope-Light", size: 48) as Any,
            NSAttributedString.Key.foregroundColor: UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1),
            NSAttributedString.Key.kern:CGFloat(5)
        ]
        let attributedTitle = NSAttributedString(string: time, attributes: attributes as! [NSAttributedString.Key : Any])
        
        weatherTimeLabel.attributedText = attributedTitle
        weatherTimeLabel.sizeToFit()
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WeatherTimeCell", for: indexPath) as? WeatherTimeCell
        cell?.fillCell(mode: items[indexPath.row].mode, time: items[indexPath.row].time)
        return cell ?? UICollectionViewCell()
    }
    

}
