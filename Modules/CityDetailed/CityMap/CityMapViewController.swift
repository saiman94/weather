//
//  CityMapViewController.swift
//  Weather v.3
//
//  Created by Артур on 14.04.2021.
//

import UIKit
import GoogleMaps

class CityMapViewController: UIViewController {
    var lat: Double
    var lon: Double
    var country: String
    
    init(title: String, lat: Double, lon: Double, country: String) {
        self.lat = lat
        self.lon = lon
        self.country = country
        super.init(nibName: "CityMapViewController", bundle: nil)
        self.title = title
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 10)
        let mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true;
            self.view = mapView

        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(lat, lon)
        marker.title = title
        marker.snippet = country
        marker.map = mapView
    }




}
