//
//  CityDetailedPresenter.swift
//  Weather v.3
//
//  Created by Артур on 30.03.2021.
//

import Foundation

class CityDetailedPresenter {
    
    weak var cityDetailedViewController: CityDetailedViewController?
    
    let id: Int
    let apiManager: CitiesApiManager
    var daySelected: Int = 0
    // переменная какой день выбран
    var nowWeather: NowWeather?
    var items: [WeatherNowItem] = []
    
    init(viewController: CityDetailedViewController, id: Int, apiManager: CitiesApiManager = ApiRequests()) {
        self.id = id
        self.apiManager = apiManager
        self.cityDetailedViewController = viewController
    }
    
    func viewIsReady() {
        apiManager.getWeekForecast(id: id, completion: { [weak self] response in
            switch response {
            case .success(let object):
                print("object tomezone: \(object.city.timezone)")
                guard let self = self else { return }
                
                self.nowWeather = object
                self.createItems()
                
                
                let dateDormatter = DateFormatter()
                dateDormatter.dateFormat = "HH:mm"
                dateDormatter.timeZone = TimeZone(secondsFromGMT: object.city.timezone)
                let time = dateDormatter.string(from: Date())
                
                self.cityDetailedViewController?.upDateView(time: time, image: object.list[0].weather[0].weatherImage, text:
                                                                object.list[0].weather[0].weatherText, name: object.city.name)
                self.cityDetailedViewController?.upDateItems(items: self.items)
                
            case .error(let errorName):
                print(errorName)
            }
        })
    }
    
    func createItems() {
        guard let nowWeather = nowWeather else { return }
        items.removeAll()
        
        let shortDateString: String
        
        let components = Calendar.current.dateComponents([.day, .month], from: nowWeather.listSortedByDays[daySelected][0].dt)
        switch components.month {
        case 1:
            shortDateString = "\(components.day!) of January"
        case 2:
            shortDateString = "\(components.day!) of February"
        case 3:
            shortDateString = "\(components.day!) of March"
        case 4:
            shortDateString = "\(components.day!) of April"
        case 5:
            shortDateString = "\(components.day!) of May"
        case 6:
            shortDateString = "\(components.day!) of June"
        case 7:
            shortDateString = "\(components.day!) of July"
        case 8:
            shortDateString = "\(components.day!) of August"
        case 9:
            shortDateString = "\(components.day!) of September"
        case 10:
            shortDateString = "\(components.day!) of October"
        case 11:
            shortDateString = "\(components.day!) of November"
        case 12:
            shortDateString = "\(components.day!) of December"
            
        default:
            shortDateString = "Not found"
        }
        
        if daySelected == 0 {
            cityDetailedViewController?.updateDateLabelAndButtons(dateText: "Today", shouldHidePreviuosDayButton: true)
            // первый день (сегодня)
            
        } else if daySelected == nowWeather.listSortedByDays.count - 1 {
            // последний день
            cityDetailedViewController?.updateDateLabelAndButtons(dateText: shortDateString, shouldHideNextDayButton: true)
        } else {
            
            if daySelected == 1 {
                cityDetailedViewController?.updateDateLabelAndButtons(dateText: "Tomorrow")
            } else {
                cityDetailedViewController?.updateDateLabelAndButtons(dateText: shortDateString)
            }
            
            // любой день между первым и последним
        }
        nowWeather.listSortedByDays[self.daySelected].forEach {
            let timeZone = TimeZone(secondsFromGMT: nowWeather.city.timezone)!
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            dateFormatter.timeZone = timeZone
            let time = dateFormatter.string(from: $0.dt)
            
            if nowWeather.city.sunset < $0.dt, ($0.dt.timeIntervalSinceReferenceDate - nowWeather.city.sunset.timeIntervalSinceReferenceDate) < 10080 {
                
                let timeZone = TimeZone(secondsFromGMT: nowWeather.city.timezone)!
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                dateFormatter.timeZone = timeZone
                let time = dateFormatter.string(from: nowWeather.city.sunset)
                
                self.items.append(WeatherNowItem.init(time: time, mode: .sunset))
            }
            
            if nowWeather.city.sunrise < $0.dt, ($0.dt.timeIntervalSinceReferenceDate - nowWeather.city.sunrise.timeIntervalSinceReferenceDate) < 10080 {
                
                
                let timeZone = TimeZone(secondsFromGMT: nowWeather.city.timezone)!
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                dateFormatter.timeZone = timeZone
                let time = dateFormatter.string(from: nowWeather.city.sunrise)
                
                self.items.append(WeatherNowItem.init(time: time, mode: .sunrise))
            }
            
            self.items.append(WeatherNowItem(time: time, mode: .temp(temp: $0.main.temp)))
        }
        cityDetailedViewController?.upDateItems(items: items)
    }
    
    
    // метод для обработки нажатия на стрелки
    func nextDayButtonDidTapped() {
        daySelected += 1
        createItems()
    }
    
    func previousDayButtonDidTapped() {
        daySelected -= 1
        createItems()
    }
    
    func locationBarButtonDidTapped() {
        let cityMapViewController = CityMapViewController(title: nowWeather?.city.name ?? "No data", lat: nowWeather?.city.coord.lat ?? 0, lon: nowWeather?.city.coord.lon ?? 0, country: nowWeather?.city.country ?? "No data")
        cityDetailedViewController?.navigationController?.pushViewController(cityMapViewController, animated: true)
    }
}

struct WeatherNowItem {
    let time: String
    let mode: WeatherTimeCellMode
}
