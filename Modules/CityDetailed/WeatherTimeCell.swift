//
//  WeatherTimeCell.swift
//  Weather v.3
//
//  Created by Артур on 04.04.2021.
//

import UIKit

enum WeatherTimeCellMode {
    case temp(temp: Float)
    case sunset
    case sunrise
}

class WeatherTimeCell: UICollectionViewCell {
    
//    var temp: Float
    @IBOutlet weak var tempWeatherLabel: UILabel!
    @IBOutlet weak var timeWeatherLabel: UILabel!
    @IBOutlet weak var sunSetAndRiseImage: UIImageView!
    
    func fillCell(mode: WeatherTimeCellMode, time: String) {

        switch mode {
        case .temp(let temp):
            tempWeatherLabel.isHidden = false
            sunSetAndRiseImage.isHidden = true
            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            let date = dateFormatter.date(from: time)
//            dateFormatter.dateFormat = "HH:mm"
//            let formatedTime = dateFormatter.string(from: date ?? Date())
            timeWeatherLabel.text = time
            timeWeatherLabel.textColor = UIColor(red: 18/255, green: 63/255, blue: 84/255, alpha: 1)
            
            switch temp {
            case -100..<(-25):
                tempWeatherLabel.textColor = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1)
            case -25..<(-15):
                tempWeatherLabel.textColor = UIColor(red: 0/255, green: 87/255, blue: 255/255, alpha: 1)
            case -15..<(-5):
                tempWeatherLabel.textColor = UIColor(red: 0/255, green: 148/255, blue: 255/255, alpha: 1)
            case -5..<5:
                tempWeatherLabel.textColor = UIColor(red: 0/255, green: 194/255, blue: 255/255, alpha: 1)
            case 5..<15:
                tempWeatherLabel.textColor = UIColor(red: 255/255, green: 138/255, blue: 0/255, alpha: 1)
            case 15..<25:
                tempWeatherLabel.textColor = UIColor(red: 255/255, green: 107/255, blue: 0/255, alpha: 1)
            case 25..<100:
                tempWeatherLabel.textColor = UIColor(red: 255/255, green: 107/255, blue: 0/255, alpha: 1)
            default:
                tempWeatherLabel.textColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
            }
            tempWeatherLabel.text = String(Int(Double(temp).rounded())) + "°C"

        case .sunrise:
            timeWeatherLabel.text = time
            timeWeatherLabel.textColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
            tempWeatherLabel.isHidden = true
            sunSetAndRiseImage.isHidden = false
            
            sunSetAndRiseImage.image = UIImage(named: "sunRise")
            
        case .sunset:
            timeWeatherLabel.text = time
            timeWeatherLabel.textColor = UIColor(red: 88/255, green: 107/255, blue: 116/255, alpha: 1)
            tempWeatherLabel.isHidden = true
            sunSetAndRiseImage.isHidden = false
            
            sunSetAndRiseImage.image = UIImage(named: "sunSet")
        }
    }
}

