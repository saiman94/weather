//
//  LocalJsonManager.swift
//  Weather v.3
//
//  Created by Артур on 22.03.2021.
//

import Foundation
class LocalJsonManager {
    func getCities () -> [City]? {
        guard let path = Bundle.main.path(forResource: "newCity.list", ofType: "json") else {
            print("no path")
            return nil
        }
        guard let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) else {
            print("no data")
            return nil }
        
        guard let cityOfJson = try? JSONDecoder().decode([City].self, from: data) else {
            print("no cities")
            return nil
        }
        return cityOfJson
    }
}
