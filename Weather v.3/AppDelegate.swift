//
//  AppDelegate.swift
//  Weather v.3
//
//  Created by Артур on 11.03.2021.
//

import UIKit
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    
    let localJsonManager = LocalJsonManager()
    let realmManager = RealmManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey("AIzaSyDSLBeofYaWn5nM2-lglhPI3VyoJ8V98yI")
        
        let realmManager = RealmManager()
        
        if realmManager.obtain(objectType: RealmCityCheckedList.self)?.isEmpty == false {
            showCityWeatherViewController()
        } else {
            showLoadCitiesViewController()
        }

        window?.makeKeyAndVisible()
        return true
    }
    
    func showCityWeatherViewController() {
        let controller = CityWeatherViewController()
        let navigationController = UINavigationController(rootViewController: controller)
        window?.rootViewController = navigationController
    }
    
    func showLoadCitiesViewController() {
        let loadController = LoadCitiesViewController(localJsonManager: localJsonManager, realmManager: realmManager)
        loadController.citiesDidLoad = {
            self.showCityWeatherViewController()
        }
        window?.rootViewController = loadController
    }
}

