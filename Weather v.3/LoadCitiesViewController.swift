//
//  LoadCitiesViewController.swift
//  Weather v.3
//
//  Created by Артур on 18.04.2021.
//

import UIKit

class LoadCitiesViewController: UIViewController {
 
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet var loadCitiesLabel: UILabel!
    var localJsonManager: LocalJsonManager
    var realmManager: RealmManager
    var citiesDidLoad: (() -> Void)?
    
    init (localJsonManager: LocalJsonManager, realmManager: RealmManager) {
        self.localJsonManager = localJsonManager
        self.realmManager = realmManager
        super.init(nibName: "LoadCitiesViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCitiesLabel.text = "Please, wait a few second"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadCities()
    }

    func loadCities() {
        print("loading")
        progressView.progress = 0

        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }
            
            let citiesList = CityList(from: self.localJsonManager.getCities() ?? [])
            let realmCities = citiesList.list.map { RealmCity(from: $0) }
            
            
            let numberOfIterations = 669303
            let onePercent = numberOfIterations / 100
            var iterationNumber = 0
            var currentPercent = 0
            
            let sortedRealmCities = realmCities.sorted {
                iterationNumber += 1
                
                if iterationNumber / onePercent != currentPercent {
                    currentPercent = iterationNumber / onePercent
                    DispatchQueue.main.async { [weak self] in
                        guard let self = self else { return }
                        self.progressView.progress = Float(currentPercent) / 100
                    }
                }
                
                return $0.country != $1.country ? $0.country < $1.country : $0.name < $1.name
            }
            
            let realmCitiesList = RealmCityList(from: sortedRealmCities)
            self.realmManager.write(object: [realmCitiesList])
            
            DispatchQueue.main.async { [weak self] in
                self?.citiesDidLoad?()
            }
        }
    }
}
